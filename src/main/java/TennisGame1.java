
public class TennisGame1 implements TennisGame {

	public static final String SCORE_LOVE = "Love";
	public static final String SCORE_FIFTEEN = "Fifteen";
	public static final String SCORE_THIRTY = "Thirty";
	public static final String SCORE_FORTY = "Forty";
	public static final String SCORE_LOVE_ALL = "Love-All";
	public static final String SCORE_FIFTEEN_ALL = "Fifteen-All";
	public static final String SCORE_THIRTY_ALL = "Thirty-All";
	public static final String SCORE_DEUCE = "Deuce";
	public static final String SCORE_ADVANTAGE_FOR = "Advantage ";
	public static final String SCORE_WIN_FOR = "Win for ";

	private final class Player {
		private final String name;
		private int score;

		public Player(String name) {
			this.name = name;
			this.score = 0;
		}

		public String getName() {
			return this.name;
		}

		public int getScore() {
			return this.score;
		}

		public void increaseScore() {
			this.score += 1;
		}
	}

	private final Player player1;
	private final Player player2;

	public TennisGame1(String player1Name, String player2Name) {
		this.player1 = new Player(player1Name);
		this.player2 = new Player(player2Name);
	}

	public void wonPoint(String playerName) {
		if (this.player1.getName().equalsIgnoreCase(playerName)) {
			this.player1.increaseScore();
		}

		if (this.player2.getName().equalsIgnoreCase(playerName)) {
			this.player2.increaseScore();
		}
	}

	public String getScore() {
		if (this.areEqualsPoints()) {
			return this.getScoreWithEqualPoints();
		}

		if (this.isAdvantageForSomePlayer()) {
			return this.getScoreWithAdvantage();
		}

		return this.getRunningScore();
	}

	private boolean areEqualsPoints() {
		return this.player1.getScore() == this.player2.getScore();
	}

	private String getScoreWithEqualPoints() {
		String result;

		switch (this.player1.getScore()) {
		case 0:
			result = SCORE_LOVE_ALL;
			break;
		case 1:
			result = SCORE_FIFTEEN_ALL;
			break;
		case 2:
			result = SCORE_THIRTY_ALL;
			break;
		default:
			result = SCORE_DEUCE;
			break;

		}
		return result;
	}

	private boolean isAdvantageForSomePlayer() {
		return !this.areEqualsPoints() && (this.player1.getScore() >= 4 || this.player2.getScore() >= 4);
	}

	private String getScoreWithAdvantage() {
		int advantageDifference = this.player1.getScore() - this.player2.getScore();

		if (advantageDifference == 1) {
			return SCORE_ADVANTAGE_FOR + this.player1.getName();
		}

		if (advantageDifference == -1) {
			return SCORE_ADVANTAGE_FOR + this.player2.getName();
		}

		if (advantageDifference >= 2) {
			return SCORE_WIN_FOR + this.player1.getName();
		}

		return SCORE_WIN_FOR + this.player2.getName();

	}

	private String getRunningScore() {
		return this.userScoreToString(this.player1.getScore()) + "-" + this.userScoreToString(this.player2.getScore());
	}

	private String userScoreToString(int score) {
		String result;

		switch (score) {
		case 0:
			result = SCORE_LOVE;
			break;
		case 1:
			result = SCORE_FIFTEEN;
			break;
		case 2:
			result = SCORE_THIRTY;
			break;
		default:
			result = SCORE_FORTY;
			break;

		}
		return result;
	}

}
