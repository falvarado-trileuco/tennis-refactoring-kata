package com.trileuco.tennis_refactoring_kata;

public final class TennisMatch {

	private static final String SCORE_SEPARATOR = "-";
	private static final String SCORE_LOVE = "All";
	private static final String SCORE_DEUCE = "Deuce";
	private static final String SCORE_ADVANTAGE_FOR = "Advantage ";
	private static final String SCORE_WIN_FOR = "Win for ";

	private Player player1;
	private Player player2;

	public TennisMatch(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
	}

	public String getScore() {
		if (this.areEqualsPoints()) {
			return this.getScoreWithEqualPoints();
		}

		if (this.isAdvantageForSomePlayer()) {
			return this.getScoreWithAdvantage();
		}

		return this.getRunningScore();
	}

	public void playerWonPoint(String playerName) {
		if (this.player1.getName().equalsIgnoreCase(playerName)) {
			this.player1.wonPoint();
		}

		if (this.player2.getName().equalsIgnoreCase(playerName)) {
			this.player2.wonPoint();
		}
	}

	private boolean areEqualsPoints() {
		return this.player1.isTiedWith(this.player2);
	}

	private boolean isAdvantageForSomePlayer() {
		return !this.areEqualsPoints()
				&& (this.player1.getScore().isMoreThanForty() || this.player2.getScore().isMoreThanForty());
	}

	private String getScoreWithEqualPoints() {
		if (this.areEqualsPoints()) {
			return this.player1.getScore().isLessThanForty()
					? this.player1.getScore().toHumanScore() + SCORE_SEPARATOR + SCORE_LOVE
					: SCORE_DEUCE;
		}

		return null;
	}

	private String getScoreWithAdvantage() {
		if (this.isAdvantageForSomePlayer()) {
			return this.player1.getScore().isMoreThan(this.player2.getScore())
					? this.getScoreInAdvantageOrWinForFirstPlayer(this.player1, this.player2)
					: this.getScoreInAdvantageOrWinForFirstPlayer(this.player2, this.player1);
		}

		return null;

	}

	private String getScoreInAdvantageOrWinForFirstPlayer(Player player1, Player player2) {
		if (player1.isInAdvantageTo(player2) || player1.wonTo(player2)) {
			if (player1.isInAdvantageTo(player2)) {
				return SCORE_ADVANTAGE_FOR + player1.getName();
			}

			if (player1.wonTo(player2)) {
				return SCORE_WIN_FOR + player1.getName();
			}
		}

		return null;
	}

	private String getRunningScore() {
		return this.player1.getScore().toHumanScore() + SCORE_SEPARATOR + this.player2.getScore().toHumanScore();
	}

}
