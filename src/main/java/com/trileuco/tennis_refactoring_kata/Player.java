package com.trileuco.tennis_refactoring_kata;

public final class Player {
	private final String name;
	private PlayerScore score;

	public Player(String name) {
		this.name = name;
		this.score = new PlayerScore(0);
	}

	public String getName() {
		return this.name;
	}

	public PlayerScore getScore() {
		return this.score;
	}

	public void wonPoint() {
		this.score.increaseScore();
	}

	public boolean isInAdvantageTo(Player otherPlayer) {
		return this.score.isDifferenceToAdvantage(otherPlayer.getScore());
	}

	public boolean wonTo(Player otherPlayer) {
		return this.score.isDifferenceToWin(otherPlayer.getScore());
	}

	public boolean isTiedWith(Player otherPlayer) {
		return this.score.isEqualsTo(otherPlayer.getScore());
	}
}
