package com.trileuco.tennis_refactoring_kata;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}