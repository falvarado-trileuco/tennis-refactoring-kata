package com.trileuco.tennis_refactoring_kata;

public class TennisGameImpl implements TennisGame {

	private TennisMatch match;

	public TennisGameImpl(String player1Name, String player2Name) {
    	this.match = new TennisMatch(new Player(player1Name), new Player(player2Name));
    }

    public String getScore() {
       return this.match.getScore();
    }

    public void wonPoint(String playerName) {
		this.match.playerWonPoint(playerName);
	}

}
