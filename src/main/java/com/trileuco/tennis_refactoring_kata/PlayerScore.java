package com.trileuco.tennis_refactoring_kata;

public final class PlayerScore {

	private static final String SCORE_LOVE = "Love";
	private static final String SCORE_FIFTEEN = "Fifteen";
	private static final String SCORE_THIRTY = "Thirty";
	private static final String SCORE_FORTY = "Forty";

	private int score;

	public PlayerScore(int score) {
		this.score = score;
	}

	public void increaseScore() {
		this.score++;
	}

	public String toHumanScore() {
		String humanScore;

		switch (this.score) {
		case 0:
			humanScore = SCORE_LOVE;
			break;
		case 1:
			humanScore = SCORE_FIFTEEN;
			break;
		case 2:
			humanScore = SCORE_THIRTY;
			break;
		case 3:
			humanScore = SCORE_FORTY;
			break;
		default:
			humanScore = "";
			break;
		}

		return humanScore;
	}

	public boolean isDifferenceToAdvantage(PlayerScore otherScore) {
		return this.isMoreThan(otherScore) && this.isMoreThanForty() && this.scoresDifferenceIs1(otherScore);
	}

	public boolean isDifferenceToWin(PlayerScore otherScore) {
		return this.isMoreThan(otherScore) && this.isMoreThanForty() && this.scoresDifferenceIs2OrMore(otherScore);
	}

	public boolean isMoreThan(PlayerScore otherScore) {
		return this.score > otherScore.score;
	}

	public boolean isEqualsTo(PlayerScore otherScore) {
		return this.score == otherScore.score;
	}

	public boolean isLessThanForty() {
		return this.score < 3;
	}

	public boolean isMoreThanForty() {
		return this.score >= 4;
	}

	public boolean scoresDifferenceIs1(PlayerScore otherScore) {
		return this.score - otherScore.score == 1;
	}

	public boolean scoresDifferenceIs2OrMore(PlayerScore otherScore) {
		return this.score - otherScore.score >= 2;
	}


}
